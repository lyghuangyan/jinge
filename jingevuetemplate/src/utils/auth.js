import Cookies from 'js-cookie'
import { getCurrentMenuList } from '@/api/menu'

const TokenKey = 'vue_admin_template_token'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

export const initMenu = (router, store) => {
  if (store.state.routes.length > 0) {
    return
  }
  getCurrentMenuList().then(res => {
    if (res.code === '200') {
      // 格式化数据
      var fmtRoutes = formatRoutes(res.data, 0, '', '')
      // 添加路由
      router.addRoutes(fmtRoutes)
      // 添加到状态管理缓存中
      store.commit('initMenu', fmtRoutes)
      // store.dispatch('connect');
    }
  })
}
export let paths = ['/', '/dashboard']
export const initPaths = () => {
  paths = ['/', '/dashboard']
  window.localStorage.setItem('paths', JSON.stringify(paths))
}
export const formatRoutes = (menuList, i, startStr, beforePath) => {
  const result = []
  for (;i < menuList.length; i++) {
    const {
      code, title, icon, path, component, redirect		} = menuList[i]
    var nextPath = ''
    if (beforePath === '') {
      nextPath = path
    } else {
      nextPath = beforePath + '/' + path
    }
    if (!code.startsWith(startStr) || code.length !== startStr.length + 2) {
      continue
    }
    paths.push(nextPath)
    window.localStorage.setItem('paths', JSON.stringify(paths))
    let children = []
    if (i + 1 < menuList.length) {
      const nextCode = menuList[i + 1].code
      if (nextCode.startsWith(code)) {
        children = formatRoutes(menuList, i + 1, code, nextPath)
      }
    }
    const fm = {
      path: path,
      name: title,
      component(resolve) {
        require([component + '.vue'], resolve)
      },
      meta: {
        title: title,
        icon: icon
      },
      children: children
    }
    if (redirect !== '') {
      fm.redirect = redirect
    }
    result.push(fm)
  }
  return result
}
