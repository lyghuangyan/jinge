import request from '@/utils/request'

export function uploadFile(params) {
  return request({
    url: '/upload/uploadFile',
    method: 'post',
    params
  })
}
