import request from '@/utils/request'

export function getList(page, size, keyWord, deptName) {
  return request({
    url: '/employee/pageEmployee',
    method: 'get',
    params: {
      pageNumber: page,
      pageSize: size,
      keyWord: keyWord,
      deptName: deptName
    }
  })
}
