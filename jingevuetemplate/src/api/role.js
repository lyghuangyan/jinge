import request from '@/utils/request'

export function pageRole(params) {
  return request({
    url: '/role/pageRole',
    method: 'get',
    params
  })
}

export function saveRole(params) {
  return request({
    url: '/role/saveRole',
    method: 'get',
    params
  })
}
export function deleteRole(params) {
  return request({
    url: '/role/deleteRole',
    method: 'get',
    params
  })
}

export function saveSecurity(params) {
  return request({
    url: '/role/saveSecurity',
    method: 'get',
    params
  })
}
