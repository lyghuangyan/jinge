import request from '@/utils/request'

export function login(params) {
  return request({
    url: '/login',
    method: 'post',
    params
  })
}

export function getInfo() {
  return request({
    url: '/user/getCurrentUserInfo',
    method: 'get'
  })
}

export function isLogin() {
  return request({
    url: '/security/isLogin',
    method: 'get'
  })
}

export function logout() {
  return request({
    url: '/logout',
    method: 'post'
  })
}

