import request from '@/utils/request'

export function updateHeadurl(headurl) {
  return request({
    url: '/user/updateHeadurl',
    method: 'get',
    params: {
      headurl: headurl
    }
  })
}
