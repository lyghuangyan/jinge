import request from '@/utils/request'

export function saveEmployee(params) {
  return request({
    url: '/employee/saveEmployee',
    method: 'post',
    params
  })
}

export function deleteEmployee(id) {
  return request({
    url: '/employee/deleteEmployee',
    method: 'get',
    params: {
      id: id
    }
  })
}
