import request from '@/utils/request'

export function getCurrentMenuList() {
  return request({
    url: '/menu/getCurrentMenuList',
    method: 'get'
  })
}

export function getAllMenuList() {
  return request({
    url: '/menu/getAllMenuList',
    method: 'get'
  })
}
