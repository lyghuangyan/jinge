import Vue from 'vue'
import Vuex from 'vuex'
import app from './modules/app'
import user from './modules/user'
import getters from './getters'
import { paths } from '@/utils/auth'

Vue.use(Vuex)
//状态管理
const store = new Vuex.Store({
	state:{
		routes: [],//路由
		//登录用户信息
		user:{
			headurl: window.localStorage.getItem('user' || '[]') == null ? '未登录' : JSON.parse(window.localStorage.getItem('user' || '[]')).headurl,
			role_id: window.localStorage.getItem('user' || '[]') == null ? '' : JSON.parse(window.localStorage.getItem('user' || '[]')).role_id,
			username: window.localStorage.getItem('user' || '[]') == null ? '' : JSON.parse(window.localStorage.getItem('user' || '[]')).username,
		}
	},
	mutations:{
		initMenu(state, menus){
			state.routes = menus;
		},
		login(state, user){
			state.user = user;
			window.localStorage.setItem('user', JSON.stringify(user));
		},
		logout(state){
			window.localStorage.removeItem('user');
			state.routes = [];
			//paths = ['/'];
		}
	},
  modules: {
    app, 
    user
  },
  getters
})

export default store
