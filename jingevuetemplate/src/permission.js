import router from './router'
import store from './store'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
import { isLogin } from '@/api/login'
import { initMenu, paths } from '@/utils/auth'

NProgress.configure({ showSpinner: false })// NProgress configuration

const whiteList = ['/login', '/401'] // 不重定向白名单
router.beforeEach((to, from, next) => {
  NProgress.start()
  if (whiteList.indexOf(to.path) !== -1) {
    next()
  }
  isLogin().then(response => {
    if (response.code === '200') {
      // 已登录
      if (to.path === '/login') {
        next({ path: '/' })
        NProgress.done() // if current page is dashboard will not trigger	afterEach hook, so manually handle it
      } else {
        // 在这里可以处理用户权限
        initMenu(router, store)
        let localPath = window.localStorage.getItem('paths' || '[]')

        if (localPath === undefined || localPath == null || localPath.length === 0) {
          localPath = paths
        } else {
          if (localPath.length < paths.length) {
            localPath = paths
          }
        }
        console.info(localPath)
        if (localPath.indexOf(to.path) !== -1) {
          next()
        } else {
          next('/401') // 没有权限的页面
          NProgress.done()
        }
        /* if(paths.indexOf(to.path) !== -1){
					next()
				}else{
					next('/401') // 没有权限的页面
					NProgress.done()
				}*/
      }
    } else {
      // 未登陆用户只能跳转白名单页面，否则强制转到登陆页
      if (whiteList.indexOf(to.path) !== -1) {
        next()
      } else {
        next('/login')
        // next(`/login?redirect=${to.path}`) // 不在白名单内且未登陆状态下全部重定向到登录页
        NProgress.done()
      }
    }
  })
  /* if (getToken()) {
    if (to.path === '/login') {
      next({ path: '/' })
      NProgress.done() // if current page is dashboard will not trigger	afterEach hook, so manually handle it
    } else {
      if (store.getters.roles.length === 0) {
        store.dispatch('GetInfo').then(res => { // 拉取用户信息
          next()
        }).catch((err) => {
          store.dispatch('FedLogOut').then(() => {
            Message.error(err || 'Verification failed, please login again')
            next({ path: '/' })
          })
        })
      } else {
        next()
      }
    }
  } else {
    if (whiteList.indexOf(to.path) !== -1) {
      next()
    } else {
      next(`/login?redirect=${to.path}`) // 否则全部重定向到登录页
      NProgress.done()
    }
  }*/
})

router.afterEach(() => {
  NProgress.done() // 结束Progress
})
