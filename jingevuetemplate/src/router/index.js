import Vue from 'vue'
import Router from 'vue-router'

// in development-env not use lazy-loading, because lazy-loading too many pages will cause webpack hot update too slow. so only in production use lazy-loading;
// detail: https://panjiachen.github.io/vue-element-admin-site/#/lazy-loading

Vue.use(Router)

/* Layout */
import Layout from '../views/layout/Layout'

/**
* hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
* alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
*                                if not set alwaysShow, only more than one route under the children
*                                it will becomes nested mode, otherwise not show the root menu
* redirect: noredirect           if `redirect:noredirect` will no redirect in the breadcrumb
* name:'router-name'             the name is used by <keep-alive> (must set!!!)
* meta : {
    title: 'title'               the name show in subMenu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    breadcrumb: false            if false, the item will hidden in breadcrumb(default is true)
  }
**/
// 挂载默认路由
export const constantRouterMap = [
  { path: '/login', component: () => import('@/views/login/index'), hidden: true },
  { path: '/404', component: () => import('@/views/404'), hidden: true },
  { path: '/401', component: () => import('@/views/errorPage/401'), hidden: true },
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    name: 'Dashboard',
    hidden: true,
    children: [{
      path: 'dashboard',
      component: () => import('@/views/dashboard/index')
    }]
  },
  /* { path: '/example', component: () => import('@/views/layout/Layout'), hidden: true },
{ path: '/form/index', component: () => import('@/views/form/index'), hidden: true },
{ path: '/nested', component: () => import('@/views/layout/Layout'), hidden: true },
{ path: 'external-link', component: () => import('@/views/layout/Layout'), hidden: true },
{ path: '/example/table', component: () => import('@/views/table/index'), hidden: true },
{ path: '/example/tree', component: () => import('@/views/tree/index'), hidden: true },
{ path: '/example/tinymce', component: () => import('@/views/tinymce/index'), hidden: true },
{ path: '/example/avatarUpload', component: () => import('@/views/upload/avatarUpload'), hidden: true },
{ path: '/nested/menu1', component: () => import('@/views/nested/menu1/index'), hidden: true },
{ path: '/nested/menu1/menu1-1', component: () => import('@/views/nested/menu1/menu1-1/index'), hidden: true },
{ path: '/nested/menu1/menu1-2', component: () => import('@/views/nested/menu1/menu1-2/index'), hidden: true },
{ path: '/nested/menu1/menu1-3', component: () => import('@/views/nested/menu1/menu1-3/index'), hidden: true },
{ path: '/nested/menu2', component: () => import('@/views/nested/menu2/index'), hidden: true },
{ path: '/nested/menu1/menu1-2/menu1-2-1', component: () => import('@/views/nested/menu1/menu1-2/menu1-2-1/index'), hidden: true },
{ path: '/nested/menu1/menu1-2/menu1-2-2', component: () => import('@/views/nested/menu1/menu1-2/menu1-2-2/index'), hidden: true },
*/ {
    path: '/example',
    component: Layout,
    redirect: '/example/table',
    name: '示例',
    meta: { title: '示例', icon: 'example' },
    children: [
      {
        path: 'table',
        name: '员工信息（表格）',
        component: () => import('@/views/table/index'),
        // meta: { title: '员工信息（表格）', icon: 'table' }
        meta: { title: '员工信息（表格）' }
      },
      {
        path: 'tree',
        name: 'Tree',
        component: () => import('@/views/tree/index'),
        // meta: { title: 'Tree', icon: 'tree' }
        meta: { title: '树' }
      },
      {
        path: 'tinymce',
        name: 'Tinymce',
        component: () => import('@/views/tinymce/index'),
        meta: { title: '富文本编辑框' }
      },
      {
        path: 'avatarUpload',
        name: 'AvatarUpload',
        component: () => import('@/views/upload/avatarUpload'),
        meta: { title: '上传头像' }
      }
    ]
  },

  {
    path: '/form',
    component: Layout,
    children: [
      {
        path: 'index',
        name: 'Form',
        component: () => import('@/views/form/index.vue'),
        meta: { title: '表格', icon: 'form' }
      }
    ]
  },

  {
    path: '/nested',
    component: Layout,
    redirect: '/nested/menu1',
    name: 'Nested',
    meta: {
      title: 'Nested',
      icon: 'nested'
    },
    children: [
      {
        path: 'menu1',
        component: () => import('@/views/nested/menu1/index'), // Parent router-view
        name: 'Menu1',
        meta: { title: 'Menu1' },
        children: [
          {
            path: 'menu1-1',
            component: () => import('@/views/nested/menu1/menu1-1'),
            name: 'Menu1-1',
            meta: { title: 'Menu1-1' }
          },
          {
            path: 'menu1-2',
            component: () => import('@/views/nested/menu1/menu1-2'),
            name: 'Menu1-2',
            meta: { title: 'Menu1-2' },
            children: [
              {
                path: 'menu1-2-1',
                component: () => import('@/views/nested/menu1/menu1-2/menu1-2-1'),
                name: 'Menu1-2-1',
                meta: { title: 'Menu1-2-1' }
              },
              {
                path: 'menu1-2-2',
                component: () => import('@/views/nested/menu1/menu1-2/menu1-2-2'),
                name: 'Menu1-2-2',
                meta: { title: 'Menu1-2-2' }
              }
            ]
          },
          {
            path: 'menu1-3',
            component: () => import('@/views/nested/menu1/menu1-3'),
            name: 'Menu1-3',
            meta: { title: 'Menu1-3' }
          }
        ]
      },
      {
        path: 'menu2',
        component: () => import('@/views/nested/menu2/index'),
        meta: { title: 'menu2' }
      }
    ]
  },

  {
    path: '/system',
    redirect: '/system/role',
    component: Layout,
    meta: { title: '系统设置', icon: 'link' },
    children: [
      {
        path: 'role',
        name: 'role',
        component: () => import('@/views/role/index'),
        meta: { title: '角色管理' }
      }
    ]
  },
  {
    path: '/amap',
    component: Layout,
    children: [
      {
        path: 'index',
        name: 'Amap',
        component: () => import('@/views/amap/index'),
        meta: { title: '高德地图', icon: 'eye' }
      }
    ]
  },

  { path: '*', redirect: '/404', hidden: true }
]

export default new Router({
  // mode: 'history', //后端支持可开
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap
})
