/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : MySQL
 Source Server Version : 50709
 Source Host           : 127.0.0.1:3306
 Source Schema         : jinge

 Target Server Type    : MySQL
 Target Server Version : 50709
 File Encoding         : 65001

 Date: 18/04/2019 17:14:24
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for dept
-- ----------------------------
DROP TABLE IF EXISTS `dept`;
CREATE TABLE `dept`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '部门名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dept
-- ----------------------------
INSERT INTO `dept` VALUES (1, '人事部');
INSERT INTO `dept` VALUES (2, '业务部');
INSERT INTO `dept` VALUES (3, '销售部');
INSERT INTO `dept` VALUES (4, '产品部');
INSERT INTO `dept` VALUES (5, '测试部');

-- ----------------------------
-- Table structure for employee
-- ----------------------------
DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '员工编号',
  `name` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '员工姓名',
  `dept_name` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '所在部门',
  `base_pay` int(11) NOT NULL DEFAULT 0 COMMENT '基本工资',
  `bonus` int(11) NOT NULL DEFAULT 0 COMMENT '奖金',
  `housing_allowance` int(11) NOT NULL DEFAULT 0 COMMENT '住房补助',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 27 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of employee
-- ----------------------------
INSERT INTO `employee` VALUES (2, '1002', '郝思嘉', '行政部', 2000, 300, 100);
INSERT INTO `employee` VALUES (3, '1003', '林晓彤', '财务部', 2500, 360, 100);
INSERT INTO `employee` VALUES (4, '1004', '曾云儿', '销售部', 2000, 450, 100);
INSERT INTO `employee` VALUES (5, '1005', '邱月清', '业务部', 3000, 120, 100);
INSERT INTO `employee` VALUES (6, '1006', '薛伟', '行政部', 2500, 300, 100);
INSERT INTO `employee` VALUES (7, '1007', '曾毅', '行政部', 2800, 360, 100);
INSERT INTO `employee` VALUES (8, '1008', '陶宏开', '销售部', 2200, 450, 100);
INSERT INTO `employee` VALUES (9, '1009', '丁欣', '业务部', 3400, 120, 100);
INSERT INTO `employee` VALUES (10, '1010', '杨晓林', '人事部', 3300, 280, 100);
INSERT INTO `employee` VALUES (11, '1011', '杜媛媛', '财务部', 3800, 220, 100);
INSERT INTO `employee` VALUES (12, '1012', '乔小麦', '财务部', 2400, 180, 100);
INSERT INTO `employee` VALUES (13, '1013', '赵辉', '销售部', 2500, 140, 100);
INSERT INTO `employee` VALUES (14, '1014', '萧钰', '业务部', 2400, 260, 100);
INSERT INTO `employee` VALUES (15, '1015', '蔡晓蓓', '财务部', 3300, 310, 100);
INSERT INTO `employee` VALUES (16, '1016', '沈沉', '人事部', 3500, 480, 100);
INSERT INTO `employee` VALUES (17, '1017', '陈琳', '行政部', 3800, 500, 100);
INSERT INTO `employee` VALUES (18, '1018', '杨晓娟', '行政部', 3500, 330, 100);
INSERT INTO `employee` VALUES (19, '1019', '李如风', '销售部', 2200, 340, 100);
INSERT INTO `employee` VALUES (20, '1020', '张倩', '销售部', 2900, 140, 100);
INSERT INTO `employee` VALUES (21, '1021', '王世凯', '业务部', 2400, 120, 100);
INSERT INTO `employee` VALUES (22, '1022', '周开来', '业务部', 3300, 460, 100);
INSERT INTO `employee` VALUES (24, '1051', '蒋浩源', '业务部', 4500, 500, 100);
INSERT INTO `employee` VALUES (25, '9999', '洗澡', '测试部', 1888, 0, 100);
INSERT INTO `employee` VALUES (26, 'few', 'wef', '业务部', 0, 0, 100);

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '菜单编码，排序及父子关系，两位为一层',
  `title` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '菜单名称；title和name都用这个',
  `icon` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '菜单图标',
  `path` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '菜单链接',
  `second_path` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '二级链接',
  `component` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '菜单页面路径',
  `redirect` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '重定向地址，例如首页路径会自动重定向dashboard',
  `isdelete` int(4) NOT NULL DEFAULT 0 COMMENT '是否删除 0未删除 1已删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES (1, '01', '示例', 'example', '/example', '', '@/views/layout/Layout', '/example/table', 0);
INSERT INTO `menu` VALUES (2, '02', '表格', 'form', '/form/index', '', '@/views/form/index', '', 0);
INSERT INTO `menu` VALUES (3, '03', '多层菜单', 'nested', '/nested', '', '@/views/layout/Layout', '/nested/menu1', 0);
INSERT INTO `menu` VALUES (4, '04', '系统设置', 'link', '/system', '', '@/views/layout/Layout', '', 0);
INSERT INTO `menu` VALUES (5, '0101', '员工信息（表格）', '', 'table', '', '@/views/table/index', '', 0);
INSERT INTO `menu` VALUES (6, '0102', '树', '', 'tree', '', '@/views/tree/index', '', 0);
INSERT INTO `menu` VALUES (7, '0103', '富文本编辑框', '', 'tinymce', '', '@/views/tinymce/index', '', 0);
INSERT INTO `menu` VALUES (8, '0104', '上传头像', '', 'avatarUpload', '', '@/views/upload/avatarUpload', '', 0);
INSERT INTO `menu` VALUES (9, '0301', 'Menu1', '', 'menu1', '', '@/views/nested/menu1/index', '', 0);
INSERT INTO `menu` VALUES (10, '030101', 'Menu1-1', '', 'menu1-1', '', '@/views/nested/menu1/menu1-1/index', '', 0);
INSERT INTO `menu` VALUES (11, '030102', 'Menu1-2', '', 'menu1-2', '', '@/views/nested/menu1/menu1-2/index', '', 0);
INSERT INTO `menu` VALUES (12, '030103', 'Menu1-3', '', 'menu1-3', '', '@/views/nested/menu1/menu1-3/index', '', 0);
INSERT INTO `menu` VALUES (13, '0302', 'Menu2', '', 'menu2', '', '@/views/nested/menu2/index', '', 0);
INSERT INTO `menu` VALUES (14, '03010201', 'Menu1-2-1', '', 'menu1-2-1', '', '@/views/nested/menu1/menu1-2/menu1-2-1/index', '', 0);
INSERT INTO `menu` VALUES (15, '03010202', 'Menu1-2-2', '', 'menu1-2-2', '', '@/views/nested/menu1/menu1-2/menu1-2-2/index', '', 0);
INSERT INTO `menu` VALUES (16, '0401', '角色管理', '', 'role', '', '@/views/role/index', '', 0);
INSERT INTO `menu` VALUES (17, '05', '高德地图', 'eye', '/amap/index', '', '@/views/amap/index', '', 0);

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '角色名称',
  `description` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '角色描述',
  `menu_codes` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '菜单权限列表，起止逗号并以逗号隔开',
  `secured` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '后台数据权限',
  `isdelete` int(4) NOT NULL DEFAULT 0 COMMENT '是否删除 0未删除 1已删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES (1, '系统管理员', '拥有管理员权限的账号', ',01,02,03,04,0101,0102,0103,0104,0301,030101,030102,030103,0302,03010201,03010202,0401,05,', 'admin', 0);
INSERT INTO `role` VALUES (2, '普通账号', '', '01,0103,02,0301,0104,03,0302,0101,0102,030101,030103', 'normal', 0);
INSERT INTO `role` VALUES (3, '测试', '', '0301,03,030101', 'ceshi', 0);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '登录名',
  `password` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '密码',
  `headurl` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '头像地址',
  `role_id` int(11) NOT NULL COMMENT '角色id',
  `isdelete` int(4) NOT NULL DEFAULT 0 COMMENT '是否删除 0未删除 1已删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', '/file/d1e03f90-ef0b-43b1-9cde-dc67c59f3a32.png', 1, 0);
INSERT INTO `user` VALUES (2, 'test', 'e10adc3949ba59abbe56e057f20f883e', '/jingespringboot/file/baa1bae4-4e21-4414-8c7f-10e567420d32.png', 2, 0);

SET FOREIGN_KEY_CHECKS = 1;
