package com.jinge.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.mybatis.generator.internal.DefaultShellCallback;

/**
 * 新建表或者更新表结构时，修改mybatis-generator.xml文件后，执行此类
 * <table schema="" tableName="user"></table>
 * @author huangyan
 * @since 2019-4-8 10:43:40
 */
public class GeneratorSqlmap 
{
    /**
     * 
     * 概要说明 : mybatis逆向工程. <br>
     * 详细说明 : mybatis逆向工程. <br>
     *
     * @throws Exception  void 类型返回值说明
     * @see  com.jinge.util.GeneratorSqlmap#generator()
     * @author  by huangyan @ 2019年4月8日, 上午10:46:34
     */
    public void generator() throws Exception
    {

        List<String> warnings = new ArrayList<String>();
        boolean overwrite = true;
		//指定 逆向工程配置文件
        File configFile = new File(this.getClass().getResource("").getPath()+"../../../mybatis-generator.xml");
        ConfigurationParser cp = new ConfigurationParser(warnings);
        Configuration config = cp.parseConfiguration(configFile);
        DefaultShellCallback callback = new DefaultShellCallback(overwrite);
        MyBatisGenerator myBatisGenerator = new MyBatisGenerator(config,
				callback, warnings);
        myBatisGenerator.generate(null);

    } 
    /**
     * 
     * 概要说明 : mybatis逆向工程. <br>
     * 详细说明 : mybatis逆向工程. <br>
     *
     * @param args main函数默认参数
     * @throws Exception  void 类型返回值说明
     * @see  com.jinge.util.GeneratorSqlmap#main()
     * @author  by huangyan @ 2019年4月8日, 上午10:46:44
     */
    public static void main(String[] args) throws Exception 
    {
        try 
        {
            GeneratorSqlmap generatorSqlmap = new GeneratorSqlmap();
            generatorSqlmap.generator();
        } 
        catch (Exception e)
        {
            e.printStackTrace();
        }
		
    }

}
