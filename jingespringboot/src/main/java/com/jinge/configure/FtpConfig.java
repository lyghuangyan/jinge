package com.jinge.configure;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix="ftp")
@PropertySource("classpath:ftp.properties")
public class FtpConfig
{
    public String host;
    public int port;
    public String username;
    public String password;
    public String workDir;
    public String encoding;
    public String root;
    public int maxTotal;
    public int minIdel;
    public int maxIdle;
    public int maxWaitMillis;
    public String getHost()
    {
        return host;
    }
    public void setHost(String host)
    {
        this.host = host;
    }
    public int getPort()
    {
        return port;
    }
    public void setPort(int port)
    {
        this.port = port;
    }
    public String getUsername()
    {
        return username;
    }
    public void setUsername(String username)
    {
        this.username = username;
    }
    public String getPassword()
    {
        return password;
    }
    public void setPassword(String password)
    {
        this.password = password;
    }
    public String getWorkDir()
    {
        return workDir;
    }
    public void setWorkDir(String workDir)
    {
        this.workDir = workDir;
    }
    public String getEncoding()
    {
        return encoding;
    }
    public void setEncoding(String encoding)
    {
        this.encoding = encoding;
    }
    public String getRoot()
    {
        return root;
    }
    public void setRoot(String root)
    {
        this.root = root;
    }
    public int getMaxTotal()
    {
        return maxTotal;
    }
    public void setMaxTotal(int maxTotal)
    {
        this.maxTotal = maxTotal;
    }
    public int getMinIdel()
    {
        return minIdel;
    }
    public void setMinIdel(int minIdel)
    {
        this.minIdel = minIdel;
    }
    public int getMaxIdle()
    {
        return maxIdle;
    }
    public void setMaxIdle(int maxIdle)
    {
        this.maxIdle = maxIdle;
    }
    public int getMaxWaitMillis()
    {
        return maxWaitMillis;
    }
    public void setMaxWaitMillis(int maxWaitMillis)
    {
        this.maxWaitMillis = maxWaitMillis;
    }

}
