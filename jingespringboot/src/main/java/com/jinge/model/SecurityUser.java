package com.jinge.model;

import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.jinge.model.generator.User;

/**
 * 
 * 概要说明 : 用户权限实体类.  <br>
 * 详细说明 : 用户权限实体类.  <br>
 * 创建时间 : 2019年4月8日 上午11:18:47 <br>
 * @author  by huangyan
 */
public class SecurityUser extends User implements UserDetails
{
	/**
	 * serialVersionUID
	 */
    private static final long serialVersionUID = -2868075223562767919L;
    /**
     * 权限列表
     */
    private List<SimpleGrantedAuthority> authorities;
	
    public SecurityUser(User user, List<SimpleGrantedAuthority> authorities)
    {
        this.setId(user.getId());
        this.setUsername(user.getUsername());
        this.setPassword(user.getPassword());
        this.setRoleId(user.getRoleId());
        this.setAuthorities(authorities);
    }
	
    public void setAuthorities(List<SimpleGrantedAuthority> authorities)
    {
        this.authorities = authorities;
    }

    @Override
	public Collection<? extends GrantedAuthority> getAuthorities()
    {
        return authorities;
    }

    @Override
	public boolean isAccountNonExpired() 
    {
        return true;
    }

    @Override
	public boolean isAccountNonLocked()
    {
        return true;
    }

    @Override
	public boolean isCredentialsNonExpired()
    {
        return true;
    }

    @Override
	public boolean isEnabled()
    {
        return true;
    }

}
