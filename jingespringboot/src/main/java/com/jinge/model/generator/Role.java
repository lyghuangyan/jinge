package com.jinge.model.generator;

public class Role {
    private Integer id;

    private String name;

    private String description;

    private String menuCodes;

    private String secured;

    private Integer isdelete;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public String getMenuCodes() {
        return menuCodes;
    }

    public void setMenuCodes(String menuCodes) {
        this.menuCodes = menuCodes == null ? null : menuCodes.trim();
    }

    public String getSecured() {
        return secured;
    }

    public void setSecured(String secured) {
        this.secured = secured == null ? null : secured.trim();
    }

    public Integer getIsdelete() {
        return isdelete;
    }

    public void setIsdelete(Integer isdelete) {
        this.isdelete = isdelete;
    }
}