package com.jinge.model.generator;

public class Employee {
    private Integer id;

    private String number;

    private String name;

    private String deptName;

    private Integer basePay;

    private Integer bonus;

    private Integer housingAllowance;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number == null ? null : number.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName == null ? null : deptName.trim();
    }

    public Integer getBasePay() {
        return basePay;
    }

    public void setBasePay(Integer basePay) {
        this.basePay = basePay;
    }

    public Integer getBonus() {
        return bonus;
    }

    public void setBonus(Integer bonus) {
        this.bonus = bonus;
    }

    public Integer getHousingAllowance() {
        return housingAllowance;
    }

    public void setHousingAllowance(Integer housingAllowance) {
        this.housingAllowance = housingAllowance;
    }
}