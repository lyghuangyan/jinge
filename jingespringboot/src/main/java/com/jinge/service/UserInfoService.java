package com.jinge.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.jinge.dao.generator.RoleMapper;
import com.jinge.model.SecurityUser;
import com.jinge.model.generator.Role;
import com.jinge.model.generator.User;


/**
 * 
 * 概要说明 : security认证+授权.  <br>
 * 详细说明 : security认证+授权.  <br>
 * 创建时间 : 2019年4月8日 上午11:05:02 <br>
 * @author  by huangyan
 */
@Service("userInfoService")
public class UserInfoService implements UserDetailsService 
{
	@Autowired
	private UserService userService;
	@Autowired
	private RoleMapper roleMapper;
	
	/**
	 * 根据username获取用户信息
	 */
	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException 
	{
		User user = userService.findByUsername(username);
		if(user != null)
		{
			//获取到登陆信息，用户名相同默认获取第一个用户，因为注册时需要校验用户名唯一性
			List<SimpleGrantedAuthority> authorities = new ArrayList<>();
			authorities.add(new SimpleGrantedAuthority("ROLE_LOGIN"));
			Role role = roleMapper.selectByPrimaryKey(user.getRoleId());
			if(role != null)
			{
			    //controller接口权限分配
			    authorities.add(new SimpleGrantedAuthority(role.getSecured()));
			}
			return new SecurityUser(user, authorities);
		} else {
			throw new UsernameNotFoundException("用户未注册");
		}
	}

}
