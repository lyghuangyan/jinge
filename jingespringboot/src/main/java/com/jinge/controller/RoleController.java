package com.jinge.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jinge.service.RoleService;
import com.jinge.util.ApiResponse;
import com.jinge.util.JingePage;

@RestController
@RequestMapping("/role")
public class RoleController extends BaseController
{
    @Autowired
    private RoleService roleService;
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @RequestMapping("/pageRole")
    public ApiResponse pageRole(
            @RequestParam(value = "pageNumber", required = false, defaultValue = "1")
            Integer pageNumber,
            @RequestParam(value = "pageSize", required = false, defaultValue = "10")
            Integer pageSize)
    {
        JingePage jingePage = roleService.pageRole(
                pageNumber, pageSize);
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setData(jingePage);
        return apiResponse;
    }
    
    @SuppressWarnings({ "rawtypes" })
    @RequestMapping("/saveRole")
    public ApiResponse saveRole(
            @RequestParam(value = "id", required = false)Integer id,
            @RequestParam("name")String name,@RequestParam("description")String description,
            @RequestParam("secured")String secured)
    {
        roleService.saveRole(id, name, description, secured);
        ApiResponse apiResponse = new ApiResponse();
        return apiResponse;
    }
    @SuppressWarnings({ "rawtypes"})
    @RequestMapping("/deleteRole")
    public ApiResponse deleteRole(
            @RequestParam("id")Integer id)
    {
        roleService.deleteRole(id);
        ApiResponse apiResponse = new ApiResponse();
        return apiResponse;
    }
    
    @SuppressWarnings({ "rawtypes"})
    @RequestMapping("/saveSecurity")
    public ApiResponse saveSecurity(
            @RequestParam("id")Integer id,
            @RequestParam(value = "codes")String codes)
    {
        roleService.saveSecurity(id, codes);
        ApiResponse apiResponse = new ApiResponse();
        return apiResponse;
    }
}
