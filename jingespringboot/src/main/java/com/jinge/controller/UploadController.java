package com.jinge.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.jinge.util.ApiResponse;
import com.jinge.util.FtpUtil;
import com.jinge.util.RotateImage;

/**
 * 
 * 概要说明 : 上传文件接口.  <br>
 * 详细说明 : 上传文件接口.  <br>
 * 创建时间 : 2019年4月16日 下午1:33:56 <br>
 * @author  by huangyan
 */
@RestController
@RequestMapping("/upload")
public class UploadController extends BaseController {
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	
	//@Value("${file.staticAccessPath}")
    //private String staticAccessPath;
    //@Value("${file.uploadFolder}")
    //private String uploadFolder;
    //@Value("${server.servlet.context-path}")
    //private String contextPath;
    @Autowired
    private FtpUtil ftpUtil;
	
    /**
     * 上传单文件
     * @param file
     * @param request
     * @return
     */
	@SuppressWarnings("rawtypes")
    @RequestMapping("/uploadFile")
	public String uploadFile(
			@RequestParam("file") MultipartFile file) {
		logger.info("uploadFile start...");
		String filesUrl = "";
		try {
		    ApiResponse apiResponse = ftpUtil.upload(file);
			if("200".equals(apiResponse.getCode())) {
			    filesUrl = apiResponse.getData().toString();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return filesUrl;
	}
	
	@RequestMapping("/uploadImage")
    public String uploadImage(
            @RequestParam("image") MultipartFile file) {
	    return this.uploadFile(file);
        /*logger.info("uploadImage start...");
        String filesUrl = "";
        String fileName = file.getOriginalFilename();
        String suffixName = fileName.substring(fileName.lastIndexOf("."));
        String newFileName = UUID.randomUUID().toString()+suffixName;
        String filePath = uploadFolder;
        String realPath = filePath + newFileName;
        File dest = new File(realPath);
        File directory = new File(uploadFolder);
        if(!directory.isDirectory()) {
            directory.mkdir();
        }
        try {
            file.transferTo(dest);
            filesUrl = contextPath
                    +staticAccessPath.substring(0, staticAccessPath.length()-2)
                    +newFileName;
            //处理ios图片旋转
            RotateImage.fixPicture(realPath);
            //success(staticAccessPath.substring(0, staticAccessPath.length()-2)
            //      +newFileName);
        } catch (Exception e) {
            logger.error(e.getMessage());
            //successFailCode(false,null,"500","上传失败");
        }
        return filesUrl;*/
    }
	
	/**
	 * 上传多文件
	 * @param file
	 * @param request
	 * @return
	 */
	@RequestMapping("/uploadFiles")
	public List<String> uploadFiles(@RequestParam("file") MultipartFile[] files) {
		List<String> filesUrl = new ArrayList<String>();
		for(MultipartFile file : files) {
		    filesUrl.add(this.uploadFile(file));
		}
		return filesUrl;
	}
	
	/**
	 * 
	 * 概要说明 : 上传多图片. <br>
	 * 详细说明 : 上传多图片,修正图片转向. <br>
	 *
	 * @param files    文件参数
	 * @return  List<String> 类型返回值说明
	 * @see  com.jinge.controller.UploadController#uploadImages()
	 * @author  by huangyan @ 2019年4月10日, 上午11:10:11
	 */
    @RequestMapping("/uploadImages")
    public List<String> uploadImages(@RequestParam("image") MultipartFile[] files)
    {
        List<String> filesUrl = new ArrayList<String>();
        for(MultipartFile file : files) {
            filesUrl.add(this.uploadFile(file));
        }
        return filesUrl;
        /*List<String> filesUrl = new ArrayList<String>();
        for(MultipartFile file : files) {
            String fileName = file.getOriginalFilename();
            String suffixName = fileName.substring(fileName.lastIndexOf("."));
            String newFileName = UUID.randomUUID().toString()+suffixName;
            String filePath = uploadFolder;
            String realPath = filePath + newFileName;
            File dest = new File(realPath);
            File directory = new File(uploadFolder);
            if(!directory.isDirectory()) {
                directory.mkdir();
            }
            try {
                file.transferTo(dest);
                filesUrl.add(contextPath
                        +staticAccessPath.substring(0, staticAccessPath.length()-2)
                        +newFileName);
                //处理ios图片旋转
                RotateImage.fixPicture(realPath);
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        }
        //success(filesUrl);
        return filesUrl;*/
    }
	
}
