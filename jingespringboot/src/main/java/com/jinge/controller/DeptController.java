package com.jinge.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jinge.service.DeptService;
import com.jinge.util.ApiResponse;

/**
 * 
 * 概要说明 : 部门控制层.  <br>
 * 详细说明 : 部门控制层.  <br>
 * 创建时间 : 2019年4月9日 下午3:55:47 <br>
 * @author  by huangyan
 */
@RestController
@RequestMapping("/dept")
public class DeptController extends BaseController
{
    @Autowired
    private DeptService deptService;
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @RequestMapping("/listDept")
    public ApiResponse listDept()
    {
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setData(deptService.listDept());
        return apiResponse;
    }
}
